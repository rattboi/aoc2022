#!/usr/bin/env python

import sys
import re

def decode_stacks_and_commands(fname):
    with open(fname,"r") as f:
        lines = f.read().splitlines()

    separator = 0

    for i, l in enumerate(lines):
        if l.strip() == "":
            separator = i

    command_lines = lines[separator+1:]
    commands = [re.findall(r'move (\d+) from (\d+) to (\d+)', c)[0] for c in command_lines]
    commands = [(int(x), int(y), int(z)) for x, y, z in commands]

    stacks_count_line = lines[separator-1].split(' ')
    num_stacks = int(list(filter(None, stacks_count_line))[-1])
    stacks = [[] for i in range(num_stacks)]

    for i in range(separator-2, -1, -1):
        for j in range(0, len(lines[i]), 4):
            stack_idx = j // 4
            k = lines[i][j+1:j+2]
            if k.strip() != "":
                stacks[stack_idx].append(k)

    return commands, stacks

def execute_commands(commands, stacks):
    for count, src, dest in commands:
        src -= 1
        dest -= 1

        temp_stack = []
        for i in range(0, count):
            s = stacks[src].pop()
            temp_stack.append(s)

        for i in range(0, count):
            s = temp_stack.pop()
            stacks[dest].append(s)

def get_top_of_stacks(stacks):
    return ''.join([s[-1] for s in stacks])

def main():
    args = sys.argv[1:]

    if len(args) > 0:
        fname = args[0]
    else:
        fname = "sample.txt"

    commands, stacks = decode_stacks_and_commands(fname)

    execute_commands(commands, stacks)

    message = get_top_of_stacks(stacks)

    print(message)

if __name__=="__main__":
    main()

