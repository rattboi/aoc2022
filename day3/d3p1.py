#!/usr/bin/env python

import sys

def get_priority(p):
    offset = 1
    diff = ord('a')
    if ord('A') <= p <= ord('Z'):
        offset = 27
        diff = ord('A')

    return offset + (p - diff)

def decode_rucksack(line):
    half = len(line)//2
    (first, second) = (line[:half], line[half:])
    first_priorities = set([get_priority(ord(i)) for i in first])
    second_priorities = set([get_priority(ord(i)) for i in second])
    both = first_priorities.intersection(second_priorities)
    return next(iter(both))

def read_rucksack(fname):
    with open(fname,"r") as f:
        return [decode_rucksack(l.strip()) for l in f]

def main():
    args = sys.argv[1:]

    if len(args) > 0:
        fname = args[0]
    else:
        fname = "sample.txt"

    s = sum(read_rucksack(fname))

    print(s)

if __name__=="__main__":
    main()

