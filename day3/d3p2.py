#!/usr/bin/env python

import sys

def get_priority(p):
    offset = 1
    diff = ord('a')
    if ord('A') <= p <= ord('Z'):
        offset = 27
        diff = ord('A')
    return offset + (p - diff)

def find_badge(rs1, rs2, rs3):
    s1, s2, s3 = set(rs1), set(rs2), set(rs3)
    return next(iter(s1.intersection(s2).intersection(s3)))

def get_three_rucksacks(f):
    (rs1, rs2, rs3) = (f.readline().strip(), f.readline().strip(), f.readline().strip())
    if not rs1 or not rs2 or not rs3:
        return None
    badge = find_badge(rs1, rs2, rs3)
    return get_priority(ord(badge))

def read_rucksack(fname):
    s = 0
    with open(fname,"r") as f:
        while True:
            badge = get_three_rucksacks(f)
            if badge is not None:
                s += badge
            else:
                break

    return s


def main():
    args = sys.argv[1:]

    if len(args) > 0:
        fname = args[0]
    else:
        fname = "sample.txt"

    s = read_rucksack(fname)
    print(s)

if __name__=="__main__":
    main()

