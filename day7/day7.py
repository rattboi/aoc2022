#!/usr/bin/env python

import sys

def get_dir(cwd, fs):
    d_ptr = fs

    for d in cwd:
        d_ptr = d_ptr[d]
        
    return d_ptr

def generate_fs(f):
    fs  = {'/': {}}
    cwd = ['/']

    for l in f:
        l = l.strip()

        if l.startswith('$'):
            if l.startswith("$ cd"):
                _, _, dirname = l.split()
                if dirname == "/":
                    cwd = ['/']
                elif dirname == "..":
                    cwd.pop() 
                else:
                    cwd.append(dirname)
        else:
            t, fname = l.split(" ")
            d = get_dir(cwd, fs)
            if (t == "dir"):
                d[fname] = {}
            else:
                d[fname] = int(t)
    return fs

def get_dir_sizes(fs):

    def dir_size(fs):
        size = 0
        sizes = {}
    
        for f in fs:
            if type(fs[f]) == dict:
                ss, s = dir_size(fs[f])
                size += s
                sizes[f] = (ss, s)
            else:
                size += fs[f]

        return sizes, size
    
    return dir_size(fs)

def get_sizes_under_threshold(sizes, x):
    total = 0

    for s in sizes:
        dirs, size = sizes[s]
        if size < x:
            total += size
        total += get_sizes_under_threshold(dirs, x)

    return total

def get_smallest_dir_over_threshold(sizes, threshold, free):

    def over_threshold(sizes, threshold, free, over):
        for s in sizes:
            dirs, size = sizes[s]
            if (free + size > threshold):
                over.append((s,size))
            over_threshold(dirs, threshold, free, over)
        return over

    over = over_threshold(sizes, threshold, free, [])

    min_size = 10000000000000
    smallest_dir_over_threshold = ''

    for dir_name, size in over:
        if size < min_size:
            min_size = size
            smallest_dir_over_threshold = dir_name

    return (smallest_dir_over_threshold, min_size)

def main():
    args = sys.argv[1:]

    if len(args) > 0:
        fname = args[0]
    else:
        fname = "sample.txt"

    with open(fname,"r") as f:
        fs = generate_fs(f) 

    sizes, _ = get_dir_sizes(fs)

    total_sizes_under_threshold = get_sizes_under_threshold(sizes, 100000)
    print(total_sizes_under_threshold)

    disk_size = 70000000
    free = disk_size - sizes['/'][1]

    smallest_dir_over_threshold, smallest_dir_size_over_threshold = get_smallest_dir_over_threshold(sizes, 30000000, free)
    print(f"{smallest_dir_over_threshold}: {smallest_dir_size_over_threshold}")

if __name__=="__main__":
    main()
