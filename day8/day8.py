#!/usr/bin/env python

import sys
from functools import reduce

def read_treemap(fname):
    treemap = []
    with open(fname,"r") as f:
        for i,l in enumerate(f):
            treemap.append([])
            for c in l.strip():
                treemap[i].append(int(c))

    return treemap

def make_empty_array(val, y, x):
    return [[val for xx in range(0, x)] for yy in range(0, y)]

def count_visible(treemap):
    y_dim, x_dim = get_dims(treemap)

    sum = 0

    for y in range(0, y_dim):
        for x in range(0, x_dim):
            if treemap[y][x] is True:
                sum += 1

    return sum


def get_visible_trees(treemap):
    y_dim, x_dim = get_dims(treemap)

    visible = make_empty_array(False, y_dim, x_dim)

    # left
    for y in range(0, y_dim):
        tallest_tree = -1
        for x in range(0, x_dim):
            if treemap[y][x] > tallest_tree:
                visible[y][x] = True
                tallest_tree = treemap[y][x]

    # right
    for y in range(0, y_dim):
        tallest_tree = -1
        for x in range(x_dim-1, 0-1, -1):
            if treemap[y][x] > tallest_tree:
                visible[y][x] = True
                tallest_tree = treemap[y][x]

    # down
    for x in range(0, x_dim):
        tallest_tree = -1
        for y in range(0, y_dim):
            if treemap[y][x] > tallest_tree:
                visible[y][x] = True
                tallest_tree = treemap[y][x]

    # up
    for x in range(0, x_dim):
        tallest_tree = -1
        for y in range(y_dim-1, 0-1, -1):
            if treemap[y][x] > tallest_tree:
                visible[y][x] = True
                tallest_tree = treemap[y][x]

    return count_visible(visible)

def get_dims(d):
    y_dim = len(d)
    x_dim = len(d[0])
    return (y_dim, x_dim)

def walk_func(treemap, y, x, w):
    height = treemap[y][x]
    trees = 0
    x_w = x
    y_w = y
    while w['cond'](x_w, y_w):
        trees += 1
        x_w += w['inc'][0]
        y_w += w['inc'][1]
        h_w = treemap[y_w][x_w]
        if h_w >= height:
            break
    return trees

def walk(treemap, y, x):
    y_dim, x_dim = get_dims(treemap)

    walk_map = [
        {
            'cond': lambda x, y: x < (x_dim-1),
            'inc': (1, 0)
        },
        {
            'cond': lambda x, y: x > 0,
            'inc': (-1, 0)
        },
        {
            'cond': lambda x, y: y < (y_dim-1),
            'inc': (0, 1)
        },
        {
            'cond': lambda x, y: y > 0,
            'inc': (0, -1)
        }
    ]
    
    return [walk_func(treemap, y, x, w) for w in walk_map]

def get_most_scenic_score(treemap):
    y_dim, x_dim = get_dims(treemap)

    largest_value = max([
        reduce(int.__mul__, walk(treemap, y, x)) 
        for x in range(0, x_dim) 
        for y in range(0, y_dim)
    ])

    return largest_value

def main():
    args = sys.argv[1:]

    if len(args) > 0:
        fname = args[0]
    else:
        fname = "sample.txt"

    treemap = read_treemap(fname)
    num_visible = get_visible_trees(treemap)
    print(num_visible)

    most_scenic = get_most_scenic_score(treemap)
    print(most_scenic)

if __name__=="__main__":
    main()
