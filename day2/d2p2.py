#!/usr/bin/env python

import sys

def read_strat_guide(fname):
    with open(fname,"r") as f:
        return [tuple(l.strip().split(" ")) for l in f]

def run_strat(strat):
    item_points = {"X": 1, "Y": 2, "Z": 3}

    to_do = {"X": {"A": "Z", "B": "X", "C": "Y"}, #lose
             "Y": {"A": "X", "B": "Y", "C": "Z"},
             "Z": {"A": "Y", "B": "Z", "C": "X"}}

    rules = {"A": {"X": 3, "Y": 6, "Z": 0},
             "B": {"X": 0, "Y": 3, "Z": 6}, 
             "C": {"X": 6, "Y": 0, "Z": 3}}

    transform = [(o, to_do[m][o]) for o,m in strat]
    
    return [(rules[o][m] + item_points[m]) for o,m in transform]

def main():
    args = sys.argv[1:]

    if len(args) > 0:
        fname = args[0]
    else:
        fname = "sample.txt"

    strat_guide = read_strat_guide(fname)
    points = run_strat(strat_guide)
    print(f"Total points: {sum(points)}")

if __name__=="__main__":
    main()

