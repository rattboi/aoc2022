#!/usr/bin/env python

import sys

def fully_contained(pair):
    r1l, r1h = pair[0][0], pair[0][1]
    r2l, r2h = pair[1][0], pair[1][1]
    if (r1l <= r2l and r1h >= r2h):
        return True
    elif (r2l <= r1l and r2h >= r1h):
        return True
    return False

def contained_ranges(ranges):
    return [1 for pair in ranges if fully_contained(pair)]

def decode_sections(sections):
    return [(tuple(map(int, s[0].split('-'))), tuple(map(int, s[1].split('-')))) for s in sections]

def read_sections(fname):
    with open(fname,"r") as f:
        return [tuple(l.strip().split(',')) for l in f]

def main():
    args = sys.argv[1:]

    if len(args) > 0:
        fname = args[0]
    else:
        fname = "sample.txt"

    sections = read_sections(fname)
    ranges = decode_sections(sections)
    print(sum(contained_ranges(ranges)))

if __name__=="__main__":
    main()

