#!/usr/bin/env python

import sys

def main():
    args = sys.argv[1:]

    fname = args[0]
    f = open(fname,"r")

    sum = 0
    largest = 0

    for l in f.readlines():
        if l.strip() == "":
            if sum > largest:
                largest = sum
            sum = 0
        else:
            sum += int(l.strip())

    if sum > largest:
        largest = sum

    print(largest)
        
if __name__=="__main__":
    main()
