#!/usr/bin/env python

import sys

def main():
    args = sys.argv[1:]

    fname = args[0]
    f = open(fname,"r")

    s = 0
    sums = [0,0,0]

    for l in f.readlines():
        if l.strip() == "":
            if s > min(sums):
                sums.append(s)
                sums = sorted(sums, reverse=True)[:3]
                print(sums)
            s = 0
        else:
            s += int(l.strip())

    print(sums)
    print(sum(sums))
        
if __name__=="__main__":
    main()
