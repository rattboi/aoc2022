#!/usr/bin/env python

import sys

def find_n_distinct(signal, length):
    for i in range(0, len(signal)-(length-1)):
        window = set(signal[i:i+length])

        if len(window) == length:
            return i+length

def main():
    args = sys.argv[1:]

    if len(args) > 0:
        fname = args[0]
    else:
        fname = "signal1.txt"

    signal = ""
    with open(fname,"r") as f:
        signal = f.read().strip()

    print(find_n_distinct(signal, 4))
    print(find_n_distinct(signal, 14))

if __name__=="__main__":
    main()

