#!/usr/bin/env python

import sys
from math import copysign

def decode_movements(fname):
    def make_pair(l):
        d, s = l.strip().split(" ")
        return (d, int(s))

    with open(fname,"r") as f:
        return [make_pair(l) for l in f]

def move(positions, direction, steps):
    offsets = {
        "L": (-1, 0),
        "R": (+1, 0),
        "U": (0, -1),
        "D": (0, +1),
    }
    x_off, y_off = offsets[direction]

    pos_history = []
    for s in range(0, steps):
        y, x = positions[0]
        x += x_off
        y += y_off
        positions[0] = (y, x)
        for i, _ in enumerate(positions[:-1]):
            positions[i+1] = determine_new_pos(positions[i], positions[i+1])
        pos_history.append(positions[-1])

    return pos_history

def determine_new_pos(h_pos, t_pos):
    hy, hx = h_pos
    ty, tx = t_pos

    x_diff = hx - tx
    y_diff = hy - ty
    x_dir = int(copysign(1, x_diff))
    y_dir = int(copysign(1, y_diff))
    x_diff_a = abs(x_diff)
    y_diff_a = abs(y_diff)

    if x_diff_a <= 1 and y_diff_a <= 1:
        return t_pos

    if x_diff_a == 0:
        return (ty + y_dir, tx)

    if y_diff_a == 0:
        return (ty,  tx + x_dir)

    return (ty + y_dir, tx + x_dir)

def run_movements(movements, rope_length):
    pos_history = list()
    positions = [(0,0)] * rope_length

    for direction, steps in movements:
        move_history = move(positions, direction, steps)
        pos_history.extend(move_history)

    return len(set(pos_history))

def main():
    args = sys.argv[1:]

    if len(args) > 0:
        fname = args[0]
    else:
        fname = "sample.txt"

    rope_movements = decode_movements(fname)
    num_positions = run_movements(rope_movements, 2)
    print(num_positions)

    num_positions = run_movements(rope_movements, 10)
    print(num_positions)

if __name__=="__main__":
    main()
